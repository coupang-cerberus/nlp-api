package com.coupang.productmatch.nlp.api.normalization;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * @author Alan (Minkyu Cho)
 */
public class NormalizerTest {
	@Test
	public void itShouldNormalizeKoreanText() throws Exception {
		assertThat(new Normalizer().normalize("빨간 ferragamo 힐")).isEqualTo("빨간,페라가모,힐");
	}

	@Test
	public void test() throws Exception {
		System.out.println(new Normalizer().normalize("빨간 ferragamo 힐"));
		System.out.println(new Normalizer().normalize("빨간ferragamo힐"));
		System.out.println(new Normalizer().normalize("로켓배송엘라스틴샴푸"));
		System.out.println(new Normalizer().normalize("로켓배송 엘라스틴 샴푸"));
		System.out.println(new Normalizer().normalize("안녕하세요엘라스틴리블랙"));
		assertThat(new Normalizer().normalize("빨간 ferragamo 힐")).isEqualTo("빨간,페라가모,힐");
	}
}