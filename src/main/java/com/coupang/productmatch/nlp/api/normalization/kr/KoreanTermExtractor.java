package com.coupang.productmatch.nlp.api.normalization.kr;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.util.AttributeFactory;
import solr.extractor.TermExtractor;
import solr.extractor.util.CharVector;
import solr.extractor.util.NxException;

import java.io.IOException;
import java.io.Reader;
import java.util.concurrent.locks.ReentrantLock;

public final class KoreanTermExtractor extends Tokenizer
{
	private String type = "KOREAN";
	private String option = "MODEL";
	private String dicPath = "";
	private boolean isDebug = false;
	public static final int DEFAULT_BUFFER_SIZE = 10240;

	private int gramSize;
	private int pos;
	private int cacheLimitSize = 0;
	private int inLen; // length of the input AFTER trim()
	private int charsRead; // length of the input
	private boolean started;
	private int readPos = 0;
	private String inStr = null;
	private String inputString = "";

	private CacheDataSet ResMorph = null;

	private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
	private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
	private final PositionIncrementAttribute posIncAtt = addAttribute(PositionIncrementAttribute.class);

	private TermExtractor extractor = null;

	private ReentrantLock lock = null;
	private CacheDataSet resMorph = null;

	/**
	 * Creates KoreanTermExtractor with given min and max n-grams.
	 * @param input {@link Reader} holding the input to be tokenized
	 */
	public KoreanTermExtractor(Reader input) {
		super(input);
		this.termAtt.resizeBuffer(DEFAULT_BUFFER_SIZE);
	}
	public KoreanTermExtractor(AttributeFactory factory, Reader input) {
		super(factory, input);
		this.termAtt.resizeBuffer(DEFAULT_BUFFER_SIZE);
	}
	public KoreanTermExtractor(AttributeFactory factory, Reader input, int bsize) {
		super(factory, input);
		this.termAtt.resizeBuffer(bsize);
	}

	public void setLock(ReentrantLock lock)
	{
		this.lock = lock;
	}

	public void setTermObject(TermExtractor extractor, boolean debug)
	{
		this.extractor = extractor;
		this.isDebug = debug;
	}

	private CacheDataSet getMorphResult(String inStr)
	{
		CacheDataSet cds = new CacheDataSet();

		CharVector vector = new CharVector();
		try{
			if( vector == null || this.extractor == null)return null;
			{
				while (this.extractor.next(vector) == true) {
					if( vector.toString() != null)
					{
						cds.addTerm(vector.toString());
						int start = vector.getStart();
						cds.addStart(start);
						cds.addEnd(start + vector.getLength());
					}
				}
			}

		}catch(NxException e){
			e.printStackTrace();
			System.out.println("Error Query (extract.setInput) : " + inStr);
			return null;
		}catch(Exception e)
		{
			System.out.println("Error Query (extract.setInput) : " + inStr);
			e.printStackTrace();
		}
		return cds;
	}

	/** Returns the next token in the stream, or null at EOS. */
	@Override
	public synchronized boolean incrementToken() throws IOException {
		long sTime;
		long eTime;
		char[] chars = new char[DEFAULT_BUFFER_SIZE];
		char[] throwaway = new char[1024];

		if (!started) {
			if( this.isDebug == true)
				System.out.println("[COUPANG_MORPH] : Locking");
			clearAttributes();
			started = true;
			readPos = 0;
			charsRead = 0;

			sTime = System.nanoTime();

			while (charsRead < chars.length) {

				int inc = 0;
				try{
					inc = input.read(chars, charsRead, chars.length-charsRead);
				}catch(Exception e) { break;}

				if (inc == -1) break;

				charsRead += inc;
			} // end of while

			eTime = System.nanoTime();
			// remove any trailing empty strings
			inStr = new String(chars, 0, charsRead).trim();

			inLen = inStr.length();
			if (inLen == 0) { return false; }

			sTime = System.nanoTime();

			if( lock != null) lock.lock();
			try{
				this.extractor.setInput(inStr.toCharArray());
			}catch(NxException e){
				e.printStackTrace();
			}catch(Exception e)
			{
				System.out.println("Error Query (extract.setInput) : " + inStr);
				e.printStackTrace();
			}

			eTime = System.nanoTime();

			long pid = Thread.currentThread().getId();
			resMorph = getMorphResult(inStr);

			if( lock != null) lock.unlock();

		} // end of started

		long pid = Thread.currentThread().getId();

		if( resMorph == null) return false;

		if( readPos >= resMorph.getListSize()) return false;

		this.termAtt.setEmpty().append( resMorph.getTerm(readPos));
			/* 2014. 4. 23일 수정 : getStart는 형태소 분석의 사용되는 변수 */
			/*
			this.offsetAtt.setOffset(
					correctOffset(resMorph.getStart(readPos)),
					correctOffset(resMorph.getEnd(readPos)));
			*/

		if( this.isDebug == true)
		{
			System.out.println("[MORPH] :: input : " + inStr );
			System.out.println("[MORPH} :: output : " + resMorph.getTerm(readPos) + ":" + resMorph.getStart(readPos) + ":" + resMorph.getEnd(readPos));
		}
		this.offsetAtt.setOffset(
				correctOffset(resMorph.getStart(readPos)),
				correctOffset(resMorph.getEnd(readPos)));
		this.posIncAtt.setPositionIncrement(1);
		readPos ++;

		chars = null;
		throwaway = null;

		return true;
	}

	@Override
	public void end() throws IOException {
		super.end();
		final int finalOffset = correctOffset(charsRead);
		this.offsetAtt.setOffset(finalOffset, finalOffset);
	}

	@Override
	public void reset() throws IOException {
		super.reset();
		started = false;
	}
}
