package com.coupang.productmatch.nlp.api.normalization;

import com.coupang.productmatch.nlp.api.util.ResourcePathUtil;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.synonym.SynonymFilterFactory;
import org.apache.lucene.analysis.util.AbstractAnalysisFactory;
import org.apache.lucene.analysis.util.FilesystemResourceLoader;
import org.apache.lucene.util.Version;
import org.bitbucket.eunjeon.mecab_ko_lucene_analyzer.StandardTokenizerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

public class NormalizationAnalyzer extends Analyzer {
	private static final String LUCENE_VERSION = Version.LUCENE_4_10_4.toString();

	@Override
	protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
		StandardTokenizerFactory factory = getStandardTokenizerFactory();
		Tokenizer tokenizer = factory.create(reader);
		LowerCaseFilterFactory lowerCaseFilterFactory = getLowerCaseFilterFactory();
		TokenStream tokenStream = lowerCaseFilterFactory.create(tokenizer);
		SynonymFilterFactory synonymFilterFactory = getSynonymFilterFactory();
		tokenStream = synonymFilterFactory.create(tokenStream);
		return new TokenStreamComponents(tokenizer, tokenStream);
	}

	private StandardTokenizerFactory getStandardTokenizerFactory() {
		StandardTokenizerFactory factory = new StandardTokenizerFactory(new HashMap<String, String>() {{
			put("useAdjectiveAndVerbOriginalForm", "false");
			put(AbstractAnalysisFactory.LUCENE_MATCH_VERSION_PARAM, LUCENE_VERSION);
		}});
		return factory;
	}

	private SynonymFilterFactory getSynonymFilterFactory() {
		SynonymFilterFactory synonymFilterFactory = new SynonymFilterFactory(new HashMap<String, String>() {{
			put("synonyms", ResourcePathUtil.getResourcePath("synonyms/synonyms.txt"));
			put("ignoreCase", "true");
			put("expand", "false");
			put(AbstractAnalysisFactory.LUCENE_MATCH_VERSION_PARAM, LUCENE_VERSION);
		}});

		try {
			synonymFilterFactory.inform(new FilesystemResourceLoader());
		} catch (IOException e) {
			throw new RuntimeException("failed to initialize SynonymFilterFactory", e);
		}
		return synonymFilterFactory;
	}

	private LowerCaseFilterFactory getLowerCaseFilterFactory() {
		return new LowerCaseFilterFactory(new HashMap<String, String>() {{
			put(AbstractAnalysisFactory.LUCENE_MATCH_VERSION_PARAM, LUCENE_VERSION);
		}});
	}
}