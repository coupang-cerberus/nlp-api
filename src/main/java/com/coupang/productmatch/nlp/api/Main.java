package com.coupang.productmatch.nlp.api; /**
 * @author Alan (Minkyu Cho)
 */

import com.coupang.productmatch.nlp.api.json.JsonTransformer;
import com.coupang.productmatch.nlp.api.komoran.KomoranAnalyzer;
import com.coupang.productmatch.nlp.api.normalization.Normalizer;

import static spark.Spark.get;
import static spark.Spark.port;

public class Main {
	public static void main(String[] args) {
		port(8080);

		get("/", (req, res) -> "Product Matcher NLP API");

		get("/hello", (req, res) -> "Hello World");

		get("/nlp", (req, res) -> {
			String input = req.queryParams("input");
			return KomoranAnalyzer.INSTANCE.getResult(input);
		}, new JsonTransformer());

		get("/moran", (req, res) -> {
			String input = req.queryParams("input");
			return new Normalizer().normalize(input);
		}, new JsonTransformer());
	}
}
