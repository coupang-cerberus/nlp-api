package com.coupang.productmatch.nlp.api.normalization.kr;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.util.AttributeFactory;
import solr.extractor.TermExtractor;
import solr.extractor.util.CharVector;
import solr.extractor.util.NxException;

import java.io.IOException;
import java.io.Reader;

public final class KoreanTermExtractorIdx extends Tokenizer
{	
	private String type = "KOREAN";
	private String option = "MODEL";
	private String dicPath = "";
	private boolean isDebug = false;
	public static final int DEFAULT_BUFFER_SIZE = 10240;
	public static final int BUFFER_SIZE = 1024;

	private int gramSize;
	private int pos;
	private int cacheLimitSize = 0;
	private int inLen; // length of the input AFTER trim()
	private int charsRead; // length of the input
	private boolean started;
	private String inputString = "";

	private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
	private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
	private static final String LOCK="lock";

	private TermExtractor extractor = null;

	/**
	 * Creates KoreanTermExtractorIdx with given min and max n-grams.
	 * @param input {@link Reader} holding the input to be tokenized
	 */
	public KoreanTermExtractorIdx(Reader input) {
		super(input);
		this.termAtt.resizeBuffer(DEFAULT_BUFFER_SIZE);
	}


	/**
	 * Creates KoreanTermExtractorIdx with given min and max n-grams.
	 * @param factory {@link org.apache.lucene.util.AttributeFactory} to use
	 * @param input {@link Reader} holding the input to be tokenized
	 */
	public KoreanTermExtractorIdx(AttributeFactory factory, Reader input, int bsize) {
		super(factory, input);
		this.termAtt.resizeBuffer(bsize);
	}

	public void setTermObject(TermExtractor extractor, boolean debug)
	{
		this.extractor = extractor;
		this.isDebug = debug;
	}
	/*
	   public void setCacheKeyList( Vector<String> cacheList)
	   {
	   this.cacheKeyList = cacheList;
	   }
	 */

	/** Returns the next token in the stream, or null at EOS. */
	@Override
		public synchronized boolean incrementToken() throws IOException {
			long sTime;
			long eTime;	
			char[] chars = new char[DEFAULT_BUFFER_SIZE];
			char[] throwaway = new char[BUFFER_SIZE];

			synchronized(LOCK)
			{
			if (!started) {
					if( this.isDebug == true)
						System.out.println("[COUPANG_MORPH] : Locking");
					clearAttributes();
					started = true;
					charsRead = 0;

					sTime = System.nanoTime();
					// TODO: refactor to a shared readFully somewhere:

					// wjlee : query 읽는 부분 
					while (charsRead < chars.length) {
						int inc = input.read(chars, charsRead, chars.length-charsRead);
						if (inc == -1) {
							break;
						}
						charsRead += inc; // wjlee : query 저장됨 --- 색인 대상도 여기서 받음 
					}
					eTime = System.nanoTime();
					// remove any trailing empty strings 
					String inStr = new String(chars, 0, charsRead).trim();  

					if (charsRead == chars.length) {
						// Read extra throwaway chars so that on end() we
						// report the correct offset:
						while(true) {
							final int inc = input.read(throwaway, 0, throwaway.length);
							if (inc == -1) {
								break;
							}
							charsRead += inc;
						}
					}

					inLen = inStr.length();
					if (inLen == 0) { return false; }
					if( chars[0] == 0) { return false; }
/*
	"독일 보이로" 와 "독일 보이로 2014년형"이 형태소 분석 결과가 서로 다르게 나옴 
*/
					sTime = System.nanoTime();
					try{

					if( this.isDebug == true)
						System.out.println("[COUPANG_MORPH] : Query " + 
									new String(chars));

						String in = new String(chars, 0, inLen);

						in = in.replaceAll("[\\s  ]", " ");
						inputString = in;

//						System.out.println("in ----> [" + in + "]");
						 // wjlee : char arr 형태로 넘기는 곳 --> 형태소 분석기
						this.extractor.setInput(in.toCharArray());
					}catch(NxException e){
						e.printStackTrace();
					}catch(Exception e) 
					{ 
						System.out.println("Error Query (extract.setInput) : " + new String(chars));
						e.printStackTrace();
					}

					eTime = System.nanoTime();
				}

				CharVector vector = new CharVector();
				try{
						if( vector == null || this.extractor == null || 
								this.extractor.next(vector) == false) return false;	// wjlee : 형태소 분석 결과를 받아옴

				}catch(NxException e){e.printStackTrace();return false;
				}catch(Exception e) 
				{ 
					System.out.println("Error Query (extract.next) : " + new String(chars)); 
					e.printStackTrace();
				}

				if( this.isDebug == true)
					System.out.println("[COUPANG_MORPH] : Vector " + 
							vector.toString());

				//				System.out.println("out ----> [" + vector.toString() + "]");

				try{
					if( vector != null && vector.toString() != null )
					{
						this.termAtt.setEmpty().append(vector.toString());

						// wjlee : solr로 형태소 분석 결과를 넘김 ... offset값을 넘김 
						this.offsetAtt.setOffset(
								correctOffset(vector.getStart()), 
								correctOffset(vector.getStart() + vector.getLength()));
					}else {
						System.out.println("[COUPANG_MORPH IN INDEX] : "+
								"Vector : Error :------- > ["+inputString+"]");
					}
				}catch(Exception e) { }


				vector = null;

				chars = null;
				throwaway = null;
			}


			return true;
		}

	@Override
		public void end() {
			// set final offset
			final int finalOffset = correctOffset(charsRead);
			this.offsetAtt.setOffset(finalOffset, finalOffset);
		}    

	@Override
		public void reset() throws IOException {
			super.reset();
			started = false;
		}
}
