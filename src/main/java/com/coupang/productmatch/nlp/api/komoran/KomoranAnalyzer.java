package com.coupang.productmatch.nlp.api.komoran;

import com.coupang.productmatch.nlp.api.util.ResourcePathUtil;
import kr.co.shineware.nlp.komoran.core.analyzer.Komoran;
import kr.co.shineware.util.common.model.Pair;

import java.util.List;

/**
 * @author Alan (Minkyu Cho)
 */
public enum KomoranAnalyzer {
	INSTANCE;
	private final Komoran KOMORAN;

	private KomoranAnalyzer() {
		KOMORAN = new Komoran(ResourcePathUtil.getResourcePath("models"));
	}

	public List<List<Pair<String, String>>> getResult(String input) {
		return KOMORAN.analyze(input);
	}
}