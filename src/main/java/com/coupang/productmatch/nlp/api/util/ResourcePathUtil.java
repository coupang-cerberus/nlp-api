package com.coupang.productmatch.nlp.api.util;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Alan (Minkyu Cho)
 */
@Slf4j
public class ResourcePathUtil {
	public static String getResourcePath(String name) {
		try {
			return ResourcePathUtil.class.getClassLoader().getResource(name).getPath();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}
}
