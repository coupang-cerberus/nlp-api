package com.coupang.productmatch.nlp.api.normalization;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionLengthAttribute;

import java.io.IOException;
import java.util.ArrayList;

/**
 * 스트림의 끝까지 이동하면서, 토큰들을 사전순으로 정렬하고, 모두 붙여 하나의 토큰으로 만든다.<br/>
 * 4개의 속성값이 아래와 같이 설정된다. <br/>
 * <ul>
 * <li>PositionIncrementAttribute=1</li>
 * <li>PositionLengthAttribute=누적 </li>
 * <li>OffsetAttribute.endOffset=누적    </li>
 * <li>CharTermAttribute = 모든 토큰을 연결하여 하나의 token으로 만듦.</li>
 * </ul>
 * <pre>
 * e.g.) "shoes","Adidas"라는 2개의 토큰이 주어지는 경우,
 * 	- 토큰 누적(stackToken)
 * 	- 사전순 정렬(processStackedTokens)
 * 	- 속성반영(applyToAttributes)
 * 결과는 아래와 같다.
 * </pre>
 * <ul>
 * <li>CharTermAttribute = "Adidasshoes"</li>
 * <li>positionIncrementAttribute = 1</li>
 * <li>PositionLengthAttribute = 2</li>
 * <li>OffsetAttribute.endOffset = 11</li>
 * </ul>
 */
public final class MorphoNormalizationTokenFilter extends TokenFilter {
	private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
	private final PositionIncrementAttribute posIncrAtt = addAttribute(PositionIncrementAttribute.class);
	private final PositionLengthAttribute posLenAtt = addAttribute(PositionLengthAttribute.class);
	private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);

	private int positionLengthStacked;
	private int endOffsetStacked;
	private ArrayList<String> stackedTokenList = new ArrayList<>();

	protected MorphoNormalizationTokenFilter(TokenStream input) {
		super(input);
	}

	@Override
	public boolean incrementToken() throws IOException {
		if (!input.incrementToken())
			return false;

		String token;
		int positionLength;
		int endOffset;
		do {
			positionLength = posLenAtt.getPositionLength();
			endOffset = offsetAtt.endOffset();
			token = new String(termAtt.buffer(), 0, termAtt.length());

			stackToken(token, positionLength, endOffset);
		} while (input.incrementToken());
		applyToAttributes();
		return true;
	}

	@Override
	public void reset() throws IOException {
		super.reset();
		stackedTokenList.clear();
		positionLengthStacked = 0;
		endOffsetStacked = 0;
	}

	private void stackToken(String token, int positionLength, int endOffset) {
		stackedTokenList.add(token);
		positionLengthStacked = positionLengthStacked + positionLength;
		endOffsetStacked = endOffsetStacked + endOffset;
	}

	private void applyToAttributes() {
		String joinedToken = StringUtils.join(stackedTokenList.toArray(), ",");
		termAtt.setEmpty();
		termAtt.append(joinedToken);
		posIncrAtt.setPositionIncrement(1);
		posLenAtt.setPositionLength(positionLengthStacked);
		offsetAtt.setOffset(0, endOffsetStacked);
	}
}