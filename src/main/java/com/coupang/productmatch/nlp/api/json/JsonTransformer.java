package com.coupang.productmatch.nlp.api.json;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 * @author Alan (Minkyu Cho)
 */
public class JsonTransformer implements ResponseTransformer {
	private final Gson gson = new Gson();

	@Override
	public String render(Object model) {
		return gson.toJson(model);
	}

}