package com.coupang.productmatch.nlp.api.normalization;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoaderAware;
import org.apache.lucene.analysis.util.TokenFilterFactory;

import java.io.IOException;
import java.util.Map;

public class NormalizationTokenFilterFactory extends TokenFilterFactory implements ResourceLoaderAware {

	public NormalizationTokenFilterFactory(Map<String, String> args){
		super(args);
	}
	@Override
	public void inform(ResourceLoader loader) throws IOException {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public TokenStream create(TokenStream input) {
		return new NormalizationTokenFilter(input);
	}
}
