package com.coupang.productmatch.nlp.api.normalization.kr;

import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoaderAware;
import org.apache.lucene.analysis.util.TokenizerFactory;
import org.apache.lucene.util.AttributeFactory;
import solr.extractor.TermExtractor;
import solr.extractor.TermExtractorFactory;
import solr.extractor.korean.KoreanExtractorExtend;
import solr.extractor.korean.KoreanExtractorNoun;
import solr.extractor.korean.KoreanExtractorSimple;
import solr.extractor.util.NxException;

import java.io.IOException;
import java.io.Reader;
import java.util.Map;

public class KoreanTermExtractorIdxFactory extends TokenizerFactory implements ResourceLoaderAware
{
	private TermExtractor extractor = null;

	private String dicPath = "";
	private String type = "KOREAN";
	private String option = "MODEL";	
	private boolean isDebug = false;

	public KoreanTermExtractorIdxFactory(Map<String, String> args) {
		super(args);

		assureMatchVersion();
		this.dicPath = (String)args.get("dicPath");
		this.type = (String)args.get("extract_type");
		this.option = (String)args.get("extract_option");
		String tmp = (String)args.get("debug");

		if( tmp != null && tmp.equals("Y")) this.isDebug = true;

		try{
			extractor = TermExtractorFactory.createTermExtractor(this.type,
				this.option, this.dicPath);
		}catch(NxException e){e.printStackTrace();}
	}

	public KoreanTermExtractorIdx create(AttributeFactory factory, Reader input)
	{
		KoreanTermExtractorIdx kt = null;
		kt = new KoreanTermExtractorIdx(input);
		kt.setTermObject(extractor, this.isDebug);

		return kt;
	}

	@Override
	public void inform(ResourceLoader loader) throws IOException
	{
		try{
			if( extractor == null)
				extractor = TermExtractorFactory.createTermExtractor(this.type, this.option, this.dicPath);
			if(this.type.equals("KOREAN") == true)
			{
				KoreanExtractorSimple extKor = (KoreanExtractorSimple)extractor;
				extKor.reloadUserDic();
			}else if(this.type.equals("KOREAN_NOUN") == true)
			{
				KoreanExtractorNoun extNoun = (KoreanExtractorNoun)extractor;
				extNoun.reloadUserDic();
			}else if(this.type.equals("KOREAN_EXTEND") == true)
			{
				KoreanExtractorExtend extExtend = (KoreanExtractorExtend)extractor;
				extExtend.reloadUserDic();
			}

		}catch(Exception e){e.printStackTrace();}
	}
}
