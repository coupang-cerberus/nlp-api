package com.coupang.productmatch.nlp.api.normalization;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.bitbucket.eunjeon.mecab_ko_lucene_analyzer.tokenattributes.PartOfSpeechAttribute;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Normalizer {
	private static final Analyzer ANALYZER = new NormalizationAnalyzer();

	public String normalize(String input) throws IOException {
		if (StringUtils.isBlank(input)) {
			return null;
		}
		TokenStream stream = ANALYZER.tokenStream("input", input);
		CharTermAttribute termAttr = stream.addAttribute(CharTermAttribute.class);
		PartOfSpeechAttribute partAttr = stream.addAttribute(PartOfSpeechAttribute.class);

		stream.reset();
		List<String> strings = new ArrayList<>();
		while (stream.incrementToken()) {
			partAttr.partOfSpeech();
			strings.add(new String(termAttr.buffer(), 0, termAttr.length()));
		}
		stream.close();
		return StringUtils.join(strings, ",");
	}
}