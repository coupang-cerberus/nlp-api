package com.coupang.productmatch.nlp.api.normalization.kr;

import net.java.moran.morph.MoranLite;
import net.java.moran.morph.MoranLiteInterface;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.util.AttributeFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

public final class KoreanTokenizer extends Tokenizer
{
	public static final int DEFAULT_BUFFER_SIZE = 1024000;

	private int gramSize;
	private int pos;
	private int inLen; // length of the input AFTER trim()
	private int cacheLimitSize = 0;
	private int charsRead; // length of the input
	private String inStr;
	private boolean started;
	private boolean isCache;

	private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
	private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);

	MoranLite ml = null;
	private Hashtable<String, CacheDataSet> morphCache = null;
	private CacheDataSet newData = null;
	private List<String> cacheKeyList = null;
	private String cacheKey = "";
	private MoranLiteInterface mli = null;
	/**
	 * Creates KoreanTokenizer with given min and max n-grams.
	 * @param input {@link Reader} holding the input to be tokenized
	 */
	public KoreanTokenizer(Reader input) {
		super(input);
		this.termAtt.resizeBuffer(DEFAULT_BUFFER_SIZE);
	}

	/**
	 * Creates KoreanTokenizer with given min and max n-grams.
	 * @param factory {@link org.apache.lucene.util.AttributeFactory} to use
	 * @param input {@link Reader} holding the input to be tokenized
	 */
	public KoreanTokenizer(AttributeFactory factory, Reader input, int bsize) {
		super(factory, input);
		this.termAtt.resizeBuffer(bsize);
	}

	public void setMoranLite( MoranLite ml)
	{
		this.ml = ml;
	}

	public void setCacheKeyList( List<String> cacheList)
	{
		this.cacheKeyList = cacheList;
	}
	public void setCacheLimitSize( int size)
	{
		this.cacheLimitSize = size;
	}

	public void setCacheBuffer( Hashtable<String, CacheDataSet> cache)
	{
		this.morphCache = cache;
	}

	public void init()
	{
		mli = new MoranLiteInterface();
	}	

	/** Returns the next token in the stream, or null at EOS. */
	@Override
		public boolean incrementToken() throws IOException {

			if (!started) {
				clearAttributes();
				started = true;
				char[] chars = new char[DEFAULT_BUFFER_SIZE];
				charsRead = 0;
				// TODO: refactor to a shared readFully somewhere:
				while (charsRead < chars.length) {
					int inc = input.read(chars, charsRead, chars.length-charsRead);
					if (inc == -1) {
						break;
					}
					charsRead += inc;
				}
				inStr = new String(chars, 0, charsRead).trim();  // remove any trailing empty strings 

				if (charsRead == chars.length) {
					// Read extra throwaway chars so that on end() we
					// report the correct offset:
					char[] throwaway = new char[1024];
					while(true) {
						final int inc = input.read(throwaway, 0, throwaway.length);
						if (inc == -1) {
							break;
						}
						charsRead += inc;
					}
				}

				inLen = inStr.length();
				if (inLen == 0) {
					return false;
				}

				cacheKey = inStr;
				newData = morphCache.get(cacheKey);

				if( newData == null)
				{
//					System.out.print("new Data ---> [ "+inStr+" ] ");
					newData = new CacheDataSet();
					cacheKeyList.add(cacheKey);
					mli.init(chars);
					ml.analyze(mli);
				}else {
//					System.out.print("hit in cache ---> [ "+inStr+" ] ");
					isCache = true;
					pos = 0;
				}
			}

			if( pos > mli.nTerm) 
			{
				pos = 0;
				return false;
			}
		
			if( isCache == false)
			{
				if (mli.termTag[pos] >=1 && mli.termTag[pos] <=3)
				{
					termAtt.setEmpty().append( new String(	mli.input, mli.termStart[pos], mli.termLength[pos]));
					offsetAtt.setOffset(
								correctOffset(mli.termStart[pos]), 
								correctOffset(mli.termStart[pos]+mli.termLength[pos]));
	/*
					System.out.println("Morph ::::::: [" + 
								new String(	mli.input, 
									mli.termStart[pos], 
									mli.termLength[pos]) + "] --- " + 
								mli.termTag[pos] + ":offset : " + mli.termStart[pos] + ":" + (mli.termStart[pos]+mli.termLength[pos]) + "\n");
	*/
					if( morphCache.size() >= this.cacheLimitSize)
								removeHashtableRandom();
							
					newData.addTerm( new String(	mli.input, mli.termStart[pos], mli.termLength[pos]));
					newData.addStart( mli.termStart[pos]);
					newData.addEnd( mli.termStart[pos] + mli.termLength[pos]);

					morphCache.put( cacheKey, newData);

				}
			}else {
				if( pos >= newData.getListSize()) return false;

				this.termAtt.setEmpty().append(newData.getTerm(pos));
				
				this.offsetAtt.setOffset(
						correctOffset(newData.getStart(pos)), 
						correctOffset(newData.getEnd(pos)));
			}

			pos++;

			return true;
		}

		public void removeHashtableRandom()
		{
			int randomSize = morphCache.size();

			Random sRandom = new Random();
        	int randomPoint = sRandom.nextInt(randomSize);

			String key = cacheKeyList.get(randomPoint);
//			System.out.println("remove  cache ---> [ "+key+" ] " + morphCache.size());

			morphCache.remove( key);
			System.out.println("remove  cache ---> [ "+key+" ] " + morphCache.size());
		}

	@Override
		public void end() {
			// set final offset
			final int finalOffset = correctOffset(charsRead);
			this.offsetAtt.setOffset(finalOffset, finalOffset);
		}    

	@Override
		public void reset() throws IOException {
			super.reset();
			started = false;
			isCache = false;
			pos = 0;
		}
}
