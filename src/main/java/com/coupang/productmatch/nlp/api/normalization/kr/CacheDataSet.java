package com.coupang.productmatch.nlp.api.normalization.kr;

import java.util.ArrayList;
import java.util.List;

public class CacheDataSet
{
	private List<String> term = new ArrayList<String>();
	private List<Integer> start = new ArrayList<Integer>();
	private List<Integer> end = new ArrayList<Integer>();
	
	public CacheDataSet() { }

	public void addTerm(String value) { this.term.add(value); }
	public void addStart(int value) { this.start.add(value); }
	public void addEnd(int value) { this.end.add(value); }

	public String getTerm(int idx) { return this.term.get(idx); }
	public int getListSize() { return this.term.size(); }
	public int getStart(int idx) { return this.start.get(idx); }
	public int getEnd(int idx) { return this.end.get(idx); }

	public void clear()
	{
		this.term.clear();
		this.start.clear();
		this.end.clear();
	}
}
